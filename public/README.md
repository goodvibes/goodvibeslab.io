Various online resources for Goodvibes.

## appdata

Images referenced by the appdata file. For details, see:
<https://www.freedesktop.org/software/appstream/docs>

## fonts

The font [font-logos](https://github.com/lukas-w/font-logos) is used to display
the logos of various Linux distributions in the Goodvibes user documentation
<https://goodvibes.readthedocs.io/en/stable/installation.html>.

It can be updated with the following commands:

    V=v0.14

    wget https://github.com/lukas-w/font-logos/releases/download/${V:?}/font-logos-${V:?}.zip
    unzip font-logos-${V:?}.zip
    cp font-logos-${V:?}/assets/font-logos.* fonts/
    rm -fr font-logos-${V:?}*

    git add fonts/
    git commit -m "Update font-logos to ${V:?}"
